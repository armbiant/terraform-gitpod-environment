# Terraform - Training sandbox

## How to use this sandbox ?

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/stack-labs/oss/terraform-gitpod-environment)

This sandbox, open automaticaly Gitpod with VS-Code and a ready-to-use environment (docker, terraform, vs-code modules...).

## Fork this project

Fork this project to get your own worspace and push files to gitlab.com !